# create a bash script of name wordpress_script.sh
# vi wordpress_script.sh
![Screenshot_from_2023-08-03_22-11-32](/uploads/cae5b92008a50ccce9f8d540799234bd/Screenshot_from_2023-08-03_22-11-32.png)
![Screenshot_from_2023-08-03_22-12-00](/uploads/3cc05b3c9fd45279ab5af11b1795380e/Screenshot_from_2023-08-03_22-12-00.png)
![Screenshot_from_2023-08-03_22-13-11](/uploads/ff6dc1055f101b5ac8b328e1d4e052b0/Screenshot_from_2023-08-03_22-13-11.png)
![Screenshot_from_2023-08-03_22-13-26](/uploads/69099e25d8a3cffcedd47b58ddcbce14/Screenshot_from_2023-08-03_22-13-26.png)
![Screenshot_from_2023-08-03_22-13-47](/uploads/6db22fe0ff5419ddd794fee801dd7ae2/Screenshot_from_2023-08-03_22-13-47.png)

# Give executable permission to file wordpress_script.sh
# chmod +x wordpress_script.sh
# ./wordpress_script.sh create example.com
# ./wordpress_script.sh toggle example.sh
![Screenshot_from_2023-08-03_22-04-27](/uploads/2fa86cd79d3e3bcf06bd847f614f7f1b/Screenshot_from_2023-08-03_22-04-27.png)
![Screenshot_from_2023-08-03_22-04-57](/uploads/069ee5c58f2dbd641dac6005e5acae25/Screenshot_from_2023-08-03_22-04-57.png)
![Screenshot_from_2023-08-03_22-10-06](/uploads/c0e926ef9404eda4ad566cd8cd67e38d/Screenshot_from_2023-08-03_22-10-06.png)

# ./wordpress_script.sh delete example.com
![Screenshot_from_2023-08-03_22-10-33](/uploads/b8d5c970c70a4c90fc906eb4c9327d45/Screenshot_from_2023-08-03_22-10-33.png)
